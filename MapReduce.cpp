#include <iostream>
#include <mutex>
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include "MapReduce.h"
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

using namespace std;
using namespace cv;

mutex mtx;
mutex mtx2;

void wordCount(string str)
{
	// Create variables
	vector<string> unique;
	multimap<string, int> text; // use a multimap instead of a vector to easily sort pair/values
	multimap<int, string> counted;
	string temp;
	ifstream infile(str); // declare instream

	// Stream in file to vector
	while (!infile.eof())
	{
		infile >> temp;
		text.insert(pair<string, int>(temp, 1));
	}
	
	// Sift out all the unique elements in the multimap and place them in a vector
	// Use the special multimap iterator for this
	for (multimap<string, int>::iterator it = text.begin(), end = text.end(); it != end; it = text.upper_bound(it->first))
	{
		//cout << it->first << ' ' << it->second << endl;
		unique.push_back(it->first);
	}
	
	// Create a new multimap that counts all the unique elements stored in vector "unique"
	int i = 0;
	for (i = 0; i < unique.size(); i++)
	{
		counted.insert(pair<int, string>(text.count(unique[i]), unique[i]));
	}

	// Print to screen using reverse iterator
	// You need to use this special iterator to iterate through multimaps
	multimap<int, string>::reverse_iterator iter;
	for (iter = counted.rbegin(); iter != counted.rend(); ++iter)
	{
		cout << (*iter).first << " " << (*iter).second << endl;
	}
}

// Pass in vector by reference
void inputReader(string filename, vector<string> &text)
{
	// Declare instream
	ifstream infile(filename);
	string temp;

	while (!infile.eof())
	{
		infile >> temp;
		text.push_back(temp);
	}
}

// Create key/value pairs
pair<string, int> Mapper(string text)
{
	// Make the key/value pair
	pair<string, int> paired;
	paired = make_pair(text, 1);

	// Return the newly created key/value pair
	return paired;
	
}

// Make function calls to Mapper using threads
void mapCall(vector<string> text, vector<pair<string, int>> &mapped, int i, int numThreads)
{
	int j;
	
	// Have the threads run on different elements skipping over each others to avoid overlap
	for (j = i; j < text.size(); j = j + numThreads)
	{
		// The critical section, you need to lock it here so that you don't get multiple of the same key/value pairs I think..
		mtx.lock();
		mapped.push_back(Mapper(text[j]));
		mtx.unlock();
	}
}


void Reduce(vector<pair<string, int>> mapped, vector<pair<string, int>> &finalCount)
{
	// Create the necessary maps and variables
	int y = 0;
	map<string, int> unique;
	multimap<string, int> multiMapped;

	// Iterate through all the mapped elements and insert them in a map and a multimap
	// Insert them in a map to find the unique keys
	// Insert them in the multimap to use the built in count() function
	for (y = 0; y < mapped.size(); y++)
	{
		unique.insert(mapped[y]);
		multiMapped.insert(mapped[y]);
	}

	// Iterate through the unique values and use built in multimap count() function to add up all unique values of the multimap
	// Put the final count in a vector
	map<string, int>::iterator ite;
	for (ite = unique.begin(); ite != unique.end(); ++ite)
	{
		finalCount.push_back(pair<string, int>(ite->first, multiMapped.count(ite->first)));
	}
}

void output(vector<pair<string, int>> finalCount)
{
	int i = 0;
	for (i = 0; i < finalCount.size(); i++)
	{
		cout << finalCount[i].first << " " << finalCount[i].second << endl;
	}
}


// ======================================== // ======================================== //
// ======================================== // ======================================== //


void mapCall_Pixels(Mat img, vector<pair<int, int>> &mappedPixels, int ii, int nThreads)
{
	int y, x;

	// Map the number of white pixels row by row for all columns
	for (y = 0; y < img.rows; y++)
	{
		for (x = ii; x < img.cols; x = x + nThreads)
		{
			mtx2.lock();
			mappedPixels.push_back(MapperPixels((int)img.at<uchar>(y, x)));
			mtx2.unlock();
		}
	}
}

pair<int, int> MapperPixels(int point)
{
	pair<int, int> mapped;
	mapped = make_pair(point, 1);

	return mapped;
}


// We are going to reduce the different pixels now
void ReducePixels(vector<pair<int, int>> mappedPixels, vector<pair<int, int>> &finalPixels)
{
	map<int, int> uniquePixels;
	multimap<int, int> countedPixels;

	int jj = 0;

	for (jj = 0; jj < mappedPixels.size(); jj++)
	{
		uniquePixels.insert(mappedPixels[jj]);
		countedPixels.insert(mappedPixels[jj]);
	}

	map<int, int>::iterator it;
	for (it = uniquePixels.begin(); it != uniquePixels.end(); ++it)
	{
		finalPixels.push_back(pair<int, int>(it->first, countedPixels.count(it->first)));
	}

}

void outputPixels(vector<pair<int, int>> finalPixels)
{
	int ii = 0;
	
	// Iterate and pring the pixel value, and then its count
	for (ii = 0; ii < finalPixels.size(); ii++)
	{
		cout << "Pixel value: "<< finalPixels[ii].first << " Count: " << finalPixels[ii].second << endl;
	}

}