#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <map>
#include "MapReduce.h"
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>

using namespace std;
using namespace cv;

int main()
{
	// Single threaded word count of Shakespeare.txt
	//wordCount("Shakespeare.txt");


	// ===================== // Multithreaded word count // ===================== //
	string filename = "Shakespeare.txt";
	vector<string> text;

	// ===================== // Input Reader // ===================== //
	// Pass in the vector by reference (check the function declaration)
	inputReader(filename, text);

	// ===================== // Mapper // ===================== //
	const int numThreads = 4;
	thread myThreads[numThreads];

	vector<pair<string, int>> mapped; // a vector of kay/value pairs

	int i = 0;
	for (i = 0; i < numThreads; i++)
	{
		myThreads[i] = thread(mapCall, text, ref(mapped), i, numThreads);
	}

	for (i = 0; i < numThreads; i++)
	{
		myThreads[i].join();
	}

	// ===================== // Reduce // ===================== //
	vector<pair<string, int>> finalCount;

	Reduce(mapped, finalCount);


	// ===================== // Output // ===================== //
	// output(finalCount);


	// Let's try and find a different implementation
	// From OpenCV https://www.youtube.com/watch?v=LNlnjBLPJd8 a lot of tedious work
	Mat img = imread("Lenna.png");
	
	//Vec3b val = img.at<Vec3b>(10, 10);
	//cout << val;

	Mat img_gray; // create grayscale variable

	// mat2gray() for C++ with opencv
	// cvtColor(colorImg, greyscaleImg, mode);
	// Guess this doesn't work? ..
	cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);

	cv::String windowO = "original";
	cv::String windowG = "gray";

	namedWindow(windowO, WINDOW_AUTOSIZE);
	imshow(windowO, img);

	namedWindow(windowG, WINDOW_AUTOSIZE);
	imshow(windowG, img_gray);
	waitKey(0);

	// This is how to access pixel values at particular locations
	// I guess they're of type uchar
	// To see RGB channels you have to use Vec3b
	// int va = (int)img.at<uchar>(0, 0);
	// cout << va;


	// We will now use MapReduce to count the number of white pixels
	// We will make our own MapReduce for this, using new functions

	// Create threads
	const int nThreads = 4;
	thread th[nThreads]; 

	vector<pair<int, int>> mappedPixels;

	/*for (int y = 0; y < img.rows; y++)
	{
		for (int x = 0; x < img.cols; x = x + nThreads)
		{
			if ((int)img.at<uchar>(y, x) == 255)
			{
				cout << (int)img.at<uchar>(y, x);
			}
		}
	} */

	
	int ii = 0;
	for (ii = 0; ii < nThreads; ii++)
	{
		// Run the threads
		th[ii] = thread(mapCall_Pixels, img, ref(mappedPixels), ii, nThreads);
	}

	for (ii = 0; ii < nThreads; ii++)
	{
		// Join threads
		th[ii].join();
	}

	vector<pair<int, int>> finalPixels;

	ReducePixels(mappedPixels, finalPixels);
	
	outputPixels(finalPixels);

	return 0;
}