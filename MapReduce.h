#ifndef MAPREDUCE_H
#define MAPREDUCE_H

#include <iostream>
#include <opencv2\opencv.hpp>
#include <opencv2\imgproc\imgproc.hpp>

using namespace cv;
using namespace std;

// Opens a text file str and reads in words to a vector
void wordCount(string str);

// Read in text from filename, pass in the vector by reference to be modified
void inputReader(string filename, vector<string> &text);

// Makes function calls to the mapper
void mapCall(vector<string> text, vector<pair<string, int>> &mapped, int i, int numThreads);

// Maps all the keys to value 1
pair<string, int> Mapper(string text);

void Reduce(vector<pair<string, int>> mapped, vector<pair<string, int>> &finalCount);

// Print the final result
void output(vector<pair<string, int>> finalCount);


// ======================================== // ======================================== //
// ======================================== // ======================================== //

// Pixel counting map call
void mapCall_Pixels(Mat img, vector<pair<int, int>> &mappedPixels, int ii, int nThreads);

pair<int, int> MapperPixels(int point);

void ReducePixels(vector<pair<int, int>> mappedPixels, vector<pair<int, int>> &finalPixels);

void outputPixels(vector<pair<int, int>> finalPixels);

#endif